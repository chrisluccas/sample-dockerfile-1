FROM openjdk:8

ADD /webapps /webapps

WORKDIR /webapps/

CMD [ "java", "--version" ]